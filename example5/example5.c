#include <klee/klee.h>
#include <assert.h>

int mulby2 (int v)
{
  return (2 * v);
}

void testme(int x, int y)
{
  int z = mulby2(y);   // z = 2 * y
  if (z == x)    
  {
    if (x > y + 10) // 2 * y = x
    {
      assert(0);   //  (2 * y = x) ^ (x > y + 15) needs to be solved for the assertion to fail
    }
  }
}

int main() {
  int x;
  int y;
  klee_make_symbolic(&x, sizeof(x), "x");
  klee_make_symbolic(&y, sizeof(y), "y");

  testme(x, y);

  return 0;
} 