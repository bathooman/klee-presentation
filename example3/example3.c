#include <klee/klee.h>
#include <string.h>
#include <assert.h>

#define SIZE 17

int main()
{
    char txt[SIZE];

    klee_make_symbolic(txt, sizeof(txt), "txt");

    klee_assume(txt[SIZE-1] == '\0');

    if(strcmp(txt, "KLEE is Awesome!") == 0)
    {
        assert(0);
    }
}