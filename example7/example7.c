#include <klee/klee.h>
#include <assert.h>

extern int dummy(int);

int main()
{
    int a;

    klee_make_symbolic(&a, sizeof(a), "a");

    int output = dummy(a);
    assert(a == output);

    
}